<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected Task $task;

    /**
     *
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }


    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {
        $tasks = $this->task->paginate(8);

        return view('tasks.index',compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     *
     */
    public function create()
    {
        return view('tasks.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     *
     */
    public function store(CreateTaskRequest $request)
    {
        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.show',compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit($id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.edit',compact('task'));

    }

    /**
     * Update the specified resource in storage.
     *
     *
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        $task->update($request->all());
        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     */
    public function destroy($id)
    {
        $task= $this->task->findOrFail($id);
        $task->delete();

        return redirect('/tasks')->with('success');
    }
}
