@extends('layouts.app')
@section('content')
        <a href="{{'tasks/create'}}">
            <button class="btn-primary btn" >Create Task</button>
        </a>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Content</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
                <tr>
                    <th scope="row" class="col-1">{{$task->id}}</th>
                    <th class="col-3">{{$task->name}}</th>
                    <td class="col-7">{{$task->content}}</td>
                    <td>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="{{route('tasks.show',$task->id)}}"><i class="fa fa-eye"></i></a></li>
                            <li class="list-inline-item"><a href="{{route('tasks.edit',$task->id)}}"><i class="fa fa-edit"></i></a></li>
                            <li class="list-inline-item">
                                <form method="POST" action="{{route('tasks.destroy',$task->id)}}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit"><i class="fa fa-trash"></i></button>
                                </form>
                            </li>
                        </ul>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        {{$tasks->links()}}
@endsection