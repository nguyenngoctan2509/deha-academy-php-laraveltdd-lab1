@extends('layouts.app')

@section('content')
    <div class="col-lg-8">
        <form action="{{route('tasks.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="lb1">Name</label>
                <input type="text" class="form-control" name="name" id="lb1" placeholder="Enter name">
                @error('name')
                <span id="name-error" class="error text-danger"  style="display: block">{{$message}}</span>
                @enderror
            </div>
            <div class="form-group">
                <label for="lb2">Content</label>
                <input type="text" class="form-control" name="content" id="lb2" placeholder="Enter content">
            </div>

            <button type="submit" style="background: #00bcd4" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection