@extends('layouts.app')

@section('content')
    <div class= "col-lg-10 justify-content-center">
        <form>
            <div class="form-group">
                <label for="lb1">Name</label>
                <input type="text" class="form-control" name="name" readonly id="lb1" value="{{$task->name}}">
            </div>
            <div class="form-group">
                <label for="lb2">Content</label>
                <input type="text" class="form-control" name="content" readonly id="lb2" value="{{$task->content}}">
            </div>

        </form>
    </div>
@endsection