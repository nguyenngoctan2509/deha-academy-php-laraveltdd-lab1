@extends('layouts.app')

@section('content')
    <div class= "col-lg-10 justify-content-center">
        <form action="{{route('tasks.update',$task->id)}}" method="POST">
            @csrf
            @method("PUT")
            <div class="form-group">
                <label for="lb1">Name</label>
                <input type="text" class="form-control" name="name" id="lb1" value="{{$task->name}}">
                @error('name')
                <span id="name-error" class="error text-danger"  style="display: block">{{$message}}</span>
                @enderror
            </div>
            <div class="form-group">
                <label for="lb2">Content</label>
                <input type="text" class="form-control" name="content" id="lb2" value="{{$task->content}}">
            </div>

            <button type="submit" style="background: #00bcd4" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection