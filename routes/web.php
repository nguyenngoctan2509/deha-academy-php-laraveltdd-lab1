<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::get('/tasks',[\App\Http\Controllers\TaskController::class,'index'])->name('tasks.index')->middleware('auth');

Route::post('/tasks',[\App\Http\Controllers\TaskController::class,'store'])->name('tasks.store')->middleware('auth');
Route::get('/tasks/create',[\App\Http\Controllers\TaskController::class,'create'])->name('tasks.create')->middleware('auth');

Route::delete('/tasks/{id}',[\App\Http\Controllers\TaskController::class,'destroy'])->name('tasks.destroy')->middleware('auth');

Route::put('/tasks/{task}',[\App\Http\Controllers\TaskController::class,'update'])->name('tasks.update')->middleware('auth');
Route::get('tasks/edit/{id}',[\App\Http\Controllers\TaskController::class,'edit'])->name('tasks.edit')->middleware('auth');

Route::get('/tasks/{id}',[\App\Http\Controllers\TaskController::class,'show'])->name('tasks.show')->middleware('auth');