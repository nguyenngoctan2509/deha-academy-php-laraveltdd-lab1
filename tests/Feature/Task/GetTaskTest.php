<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetTaskTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_get_task_by_id()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get(route('tasks.show',$task->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
    }

    /** @test  */
    public function unauthenticated_user_cant_get_task(){
        $task = Task::factory()->create();
        $response = $this->get(route('tasks.show',$task->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_cant_get_task_if_it_not_exists(){
        $this->actingAs(User::factory()->create());
        $taskId = -1;
        $response = $this->get(route('tasks.show',$taskId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
