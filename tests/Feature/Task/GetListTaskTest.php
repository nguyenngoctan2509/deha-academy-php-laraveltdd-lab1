<?php

namespace tests\Feature\Task;

use \App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    public function getListTaskRoute(){
        return route('tasks.index');
    }

    /** @test */
    public function authenticated_user_can_get_list_task(){
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getListTaskRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
    }

    /** @test  */
    public function unauthenticated_user_cant_get_list_task(){
        $response = $this->get($this->getListTaskRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
