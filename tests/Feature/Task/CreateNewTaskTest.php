<?php

namespace Tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{
    public function getCreateTaskRoute(){
        return route('tasks.store');
    }

    public function getCreateTaskViewRoute(){
        return route('tasks.create');
    }

    /** @test  */
    public function authenticated_user_can_create_new_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateTaskRoute(),$task);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks',$task);
    }

    /** @test  */
    public function unauthenticated_user_cant_create_new_task(){
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getCreateTaskRoute(),$task);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_cant_create_new_task_if_name_is_null(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name'=>null])->toArray();
        $response = $this->post($this->getCreateTaskRoute(),$task);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticated_user_can_see_create_task_view(){
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateTaskViewRoute());

        $response->assertViewIs('tasks.create');
    }

    /** @test  */
    public function unauthenticated_user_cant_see_create_task_view(){
        $response = $this->get($this->getCreateTaskViewRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_can_see_validation_errors_message_if_data_not_valid(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name'=>null])->toArray();
        $response = $this->from($this->getCreateTaskViewRoute())->post($this->getCreateTaskRoute(),$task);

        $response->assertRedirect($this->getCreateTaskViewRoute());
    }
}
