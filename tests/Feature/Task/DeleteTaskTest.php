<?php

namespace tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_delete_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $taskBeforeDel = Task::count();
        $response = $this->delete(route('tasks.destroy',$task->id));
        $taskAfterDel = Task::count();

        $response->assertRedirect('/tasks');
        $this->assertEquals($taskBeforeDel-1,$taskAfterDel);
    }

    /** @test  */
    public function unauthenticated_user_cant_delete_task(){
        $task = Task::factory()->create();
        $response = $this->delete(route('tasks.destroy',$task->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_cant_delete_task_if_if_not_exists()
    {
        $this->actingAs(User::factory()->create());
        $taskId = -1;
        $response = $this->delete(route('tasks.destroy',$taskId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
