<?php

namespace tests\Feature\Task;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    use WithFaker;
    public function getEditTaskRoute($task){
        return route('tasks.update',$task);
    }
    public function getEditTaskViewRoute($id){
        return route('tasks.edit',$id);
    }
    /** @test  */
    public function authenticated_user_can_update_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $dataUpdate=[
            'name' => $this->faker->name,
            'content'=> $this->faker->text
        ];
        $response = $this->get($this->getEditTaskViewRoute($task->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test  */
    public function unauthenticated_user_cant_update_task()
    {
        $task = Task::factory()->create();
        $dataUpdate=[
            'name' => $this->faker->name,
            'content'=> $this->faker->text
        ];
        $response = $this->get($this->getEditTaskViewRoute($task->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_can_see_update_task_view(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();

        $response = $this->get($this->getEditTaskViewRoute($task->id));

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test  */
    public function unauthenticated_user_cant_see_update_task_view(){
        $task = Task::factory()->create();

        $response = $this->get($this->getEditTaskViewRoute($task->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');

    }

    /** @test  */
    public function authenticated_user_cant_update_task_if_name_is_null(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $dataUpdate=[
            'name' => '',
            'content'=> $this->faker->text
        ];
        $response = $this->put($this->getEditTaskRoute($task),$dataUpdate);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticated_user_can_see_validation_error_when_task_is_not_valid(){
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $dataUpdate=[
            'name' => '',
            'content'=> $this->faker->text
        ];
        $response = $this->from($this->getEditTaskViewRoute($task->id))->put($this->getEditTaskRoute($task),$dataUpdate);

        $response->assertRedirect($this->getEditTaskViewRoute($task->id));
    }
}
