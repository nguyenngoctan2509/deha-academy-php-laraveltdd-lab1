<?php

namespace Tests\Unit\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetUserTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_get_user_by_id()
    {
        $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $response = $this->get(route('users.show',$user->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.show');
    }

    /** @test  */
    public function unauthenticated_user_cant_get_user(){
        $user = User::factory()->create();
        $response = $this->get(route('users.show',$user->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_cant_get_user_if_it_not_exists(){
        $this->actingAs(User::factory()->create());
        $userId = -1;
        $response = $this->get(route('users.show',$userId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
