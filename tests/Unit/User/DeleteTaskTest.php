<?php

namespace tests\Unit\User;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_delete_user()
    {
        $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $taskBeforeDel = User::count();
        $response = $this->delete(route('users.destroy',$user->id));
        $taskAfterDel = User::count();

        $response->assertRedirect('/users');
        $this->assertEquals($taskBeforeDel-1,$taskAfterDel);
    }

    /** @test  */
    public function unauthenticated_user_cant_delete_user(){
        $user = User::factory()->create();
        $response = $this->delete(route('users.destroy',$user->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_cant_delete_user_if_if_not_exists()
    {
        $this->actingAs(User::factory()->create());
        $taskId = -1;
        $response = $this->delete(route('users.destroy',$taskId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }
}
