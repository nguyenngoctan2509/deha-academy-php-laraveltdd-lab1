<?php

namespace Tests\Unit\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewUserTest extends TestCase
{
    public function getCreateUserRoute(){
        return route('users.store');
    }

    public function getCreateUserViewRoute(){
        return route('users.create');
    }

    /** @test  */
    public function authenticated_user_can_create_new_user()
    {
        $this->actingAs(User::factory()->create());
        $user = User::factory()->make()->toArray();
        $response = $this->post($this->getCreateUserRoute(),$user);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('users',$user);
    }

    /** @test  */
    public function unauthenticated_user_cant_create_new_user(){
        $user = User::factory()->make()->toArray();
        $response = $this->post($this->getCreateUserRoute(),$user);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_cant_create_new_user_if_name_is_null(){
        $this->actingAs(User::factory()->create());
        $user = User::factory()->make(['name'=>null])->toArray();
        $response = $this->post($this->getCreateUserRoute(),$user);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticated_user_can_see_create_user_view(){
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getCreateUserViewRoute());

        $response->assertViewIs('users.create');
    }

    /** @test  */
    public function unauthenticated_user_cant_see_create_user_view(){
        $response = $this->get($this->getCreateUserViewRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_can_see_validation_errors_message_if_data_not_valid(){
        $this->actingAs(User::factory()->create());
        $user = User::factory()->make(['name'=>null])->toArray();
        $response = $this->from($this->getCreateUserViewRoute())->post($this->getCreateUserRoute(),$user);

        $response->assertRedirect($this->getCreateUserViewRoute());
    }
}
