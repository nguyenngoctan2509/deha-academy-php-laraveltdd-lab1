<?php

namespace tests\Unit\User;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use WithFaker;
    public function getEditUserRoute($user){
        return route('users.update',$user);
    }
    public function getEditUserViewRoute($id){
        return route('users.edit',$id);
    }
    /** @test  */
    public function authenticated_user_can_update_user()
    {
        $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $dataUpdate=[
            'name' => $this->faker->name,
            'description'=> $this->faker->text
        ];
        $response = $this->get($this->getEditUserViewRoute($user->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test  */
    public function unauthenticated_user_cant_update_user()
    {
        $user = User::factory()->create();
        $dataUpdate=[
            'name' => $this->faker->name,
            'description'=> $this->faker->text
        ];
        $response = $this->get($this->getEditUserViewRoute($user->id),$dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_can_see_update_user_view(){
        $this->actingAs(User::factory()->create());
        $user = User::factory()->create();

        $response = $this->get($this->getEditUserViewRoute($user->id));

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test  */
    public function unauthenticated_user_cant_see_update_user_view(){
        $user = User::factory()->create();

        $response = $this->get($this->getEditUserViewRoute($user->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');

    }

    /** @test  */
    public function authenticated_user_cant_update_user_if_name_is_null(){
        $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $dataUpdate=[
            'name' => '',
            'description'=> $this->faker->text
        ];
        $response = $this->put($this->getEditUserRoute($user),$dataUpdate);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test  */
    public function authenticated_user_can_see_validation_error_when_user_is_not_valid(){
        $this->actingAs(User::factory()->create());
        $user = User::factory()->create();
        $dataUpdate=[
            'name' => '',
            'description'=> $this->faker->text
        ];
        $response = $this->from($this->getEditUserViewRoute($user->id))->put($this->getEditUserRoute($user),$dataUpdate);

        $response->assertRedirect($this->getEditUserViewRoute($user->id));
    }
}
