<?php

namespace tests\Unit\User;

use \App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListUserTest extends TestCase
{
    public function getListUserRoute(){
        return route('users.index');
    }

    /** @test */
    public function authenticated_user_can_get_list_user(){
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getListUserRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
    }

    /** @test  */
    public function unauthenticated_user_cant_get_list_user(){
        $response = $this->get($this->getListUserRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
